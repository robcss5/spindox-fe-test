import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import User from './interfaces/user';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'spindox-fe-test';
  user$: Observable<User> | undefined;
  tabContent: string;

  constructor(private userService: UserService) {
    this.tabContent = 'nameTab';
  }

  ngOnInit(): void {
    this.user$ = this.userService.getUser();
  }

}
