export default interface User {
    name: string;
    email: string;
    birthday: string;
    address: string;
    phone: string;
    password: string;
    picture: string;
}