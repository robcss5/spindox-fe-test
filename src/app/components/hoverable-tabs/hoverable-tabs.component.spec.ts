import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HoverableTabsComponent } from './hoverable-tabs.component';

describe('HoverableTabsComponent', () => {
  let component: HoverableTabsComponent;
  let fixture: ComponentFixture<HoverableTabsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HoverableTabsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HoverableTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
