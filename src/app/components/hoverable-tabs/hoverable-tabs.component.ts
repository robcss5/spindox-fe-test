import { AfterContentInit, Component, ContentChildren, EventEmitter, Output, QueryList } from '@angular/core';
import { HoverableTabComponent } from '../hoverable-tab/hoverable-tab.component';

@Component({
  selector: 'app-hoverable-tabs',
  templateUrl: './hoverable-tabs.component.html',
  styleUrls: ['./hoverable-tabs.component.css']
})
export class HoverableTabsComponent implements AfterContentInit {
  @ContentChildren(HoverableTabComponent) tabs!: QueryList<HoverableTabComponent>;
  @Output() activatedTab = new EventEmitter<string>();

  constructor() { }

  ngAfterContentInit(): void {
    this.tabs.forEach(tab => tab.selectedTab.subscribe(tabId => this.activateTab(tabId)));
  }

  activateTab(selectedTabId: string): void {
    this.tabs.forEach(tab => {
      if (tab.tabId === selectedTabId) {
        tab.active = true;
      } else {
        tab.active = false;
      }
    });

    this.activatedTab.emit(selectedTabId);
  }

}
