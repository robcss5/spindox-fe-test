import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-hoverable-tab',
  templateUrl: './hoverable-tab.component.html',
  styleUrls: ['./hoverable-tab.component.css']
})
export class HoverableTabComponent implements OnInit {
  @Input() active = false;
  @Input() tabId = 'tab1';
  @Input() iconName: string | undefined;
  @Input() tabTitle: string | undefined;
  @Output() selectedTab = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

  emitTabSelection(): void {
    this.selectedTab.emit(this.tabId);
  }

}
