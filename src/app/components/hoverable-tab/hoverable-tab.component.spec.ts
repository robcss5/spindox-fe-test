import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HoverableTabComponent } from './hoverable-tab.component';

describe('HoverableTabComponent', () => {
  let component: HoverableTabComponent;
  let fixture: ComponentFixture<HoverableTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HoverableTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HoverableTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
