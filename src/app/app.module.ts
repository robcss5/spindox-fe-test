import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HoverableTabsComponent } from './components/hoverable-tabs/hoverable-tabs.component';
import { HoverableTabComponent } from './components/hoverable-tab/hoverable-tab.component';

@NgModule({
  declarations: [
    AppComponent,
    HoverableTabsComponent,
    HoverableTabComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
