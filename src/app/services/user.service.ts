import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import ApiResponse from '../interfaces/apiResponse';
import User from '../interfaces/user';



@Injectable({
  providedIn: 'root'
})
export class UserService {

  API_URL = 'https://randomuser.me/api';

  constructor(
    private http: HttpClient) { }

  getUser(): Observable<User> {

    const apiResponse = this.http.get(this.API_URL) as Observable<ApiResponse>;

    return apiResponse.pipe(
      map(res => {
        const result = res.results[0];

        const user: User = {
          name: `${result.name.first} ${result.name.last}`,
          email: result.email,
          birthday: new Date(result.dob.date || '01/01/1999').toLocaleDateString('it-IT'),
          address: `${result.location.street.number} ${result.location.street.name}`,
          phone: result.phone,
          password: result.login.password,
          picture: result.picture.large
        };

        return user;
      }),
      tap(user => console.log(user))
    );
  }
}
